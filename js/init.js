(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.carousel').carousel();
    $(document).ready(function(){
      $('.materialboxed').materialbox();
    });

  }); // end of document ready
})(jQuery); // end of jQuery name space
