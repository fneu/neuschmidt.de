---
layout: post
excerpt: Northern Trip part 1
---

Longest title so far - after the longest waiting period yet. I didn't take my laptop with me on the trip, but my camera, so we have pictures. I have no idea why some wouldn't show up in the last post, so it might happen again, I'm sorry.

So, anyway, I went on a trip through the northern regions of Ghana from Thursday to Sunday. The Trip was organized by the IAESTE Dennis (I will call him small Dennis from now on) and other local IAESTE Members. 21 Students were attending the trip, 13 from Accra, 1 from another city of which I forgot the name, and 7 of us 8 from Kumasi.

That's right, one guy couldn't come with us and that was Big Dennis, the swedish guy that works at my Company. He got Malaria on Wednesday, as well as a stomach infection roughly at the same time which interfered with his Malaria treatment. He is doing better now but still can't go to work with me. Poor Dennis, he missed a lot.

We had a bus and a driver rented for the duration of the trip. The bus was tiny, really tiny. Folding seats covered the centre aisle to provide enough space, and backpacks covered the floor. Much like the tro-tros in Kumasi it wasn't really built for me and we usually drove 2 hours between stops. It was not comfortable but let's concentrate on the nice stuff.

Our first stop was the Tafi Atome Monkey Sanctuary, a jungle village that is inhabited by around a hundred humans and over 2000 Monkeys. The latter live in the jungle for most of the time but they visit the village two times a day. They also show up if tourists are near since that means food for them! (and pictures for us).

![IMG_6218.jpg](/assets/img/ghana/IMG_6218.jpg){:.materialboxed}

They are not really scared of humans and so adorable when they try to get a peanut, banana or bread from you when you hold on to it.

![IMG_6215.jpg](/assets/img/ghana/IMG_6215.jpg){:.materialboxed}

The jungle forest around the village has some other interesting features, such as this hollow tree that got formed by a vine growing around a former tree.

![IMG_6183.jpg](/assets/img/ghana/IMG_6183.jpg){:.materialboxed}

![IMG_6189.jpg](/assets/img/ghana/IMG_6189.jpg){:.materialboxed}

oh, and this:
![IMG_6224.jpg](/assets/img/ghana/IMG_6224.jpg){:.materialboxed}

Afterwards, we went to a site were we could climb some "mountains". While everybody followed the guide more or les, to a special rock formation, I went ahead with two other guys. After reaching the mentioned rock, we wondered if we could actually climb the highest of the hills and we tried (the one on the left).

![IMG_6250.jpg](/assets/img/ghana/IMG_6250.jpg){:.materialboxed}

Long story short, we did it, but it was hard, slippery, and we had to fight our way through the jungle for about 50 meters. It was scary and stingy and difficult and we reached the top, but we came back to the bus 20 minutes late, sweaty and dirty.

We went straight to out hostel afterwards, were we lived in rooms of two. I took a room together with John, the austrian guy. Everybody went out to eat and drink later that night and had a fun time getting to know each other.

So all in all the trip started really nice, and I can tell you already that more good stuff happened later. It's almost 1 am already though, so you have to wait for the other 3 days.

Quick current update: Dennis is doing better but not fine. I didn't have electricity at work the whole day today, and it went off in our hostel 5 minutes after I got home. I will have to charge my laptop for the next blog post, so let's hope I can do so, soon. I really want to tell you about the other days - and work! We have a third intern now, a local girl from Kumasi. See you soon, guys!
