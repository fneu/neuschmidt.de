---
layout: post
---

And that's it. Wow.

I'm sitting in the House of Beatrice now, the nice woman that kindly provides accommodation for the IAESTE group in Accra. My plane leaves in the afternoon, there is not much to do here for me anymore. Nothing but sorting a couple of pictures and writing this blog post. So let's get to it.

<h2>Volta Trip</h2>

<h3>Thursday</h3>

On Thursday we left Beatrice's house in Accra for a last trip to a part of Ghana we have not yet seen: The Volta Region in the east of Ghana. We, that is Raffael the Austrian guy from Takoradi and the german-speaking core group from Kumasi: John, Ben, Lisa, Olga, and me.

Fortunately, a group of students from Accra made a similar tour before and gave us a couple of important tips. Unfortunately, John got Malaria on Wednesday, making it so that <strong>all guys</strong> in Kumasi had malaria now (25% of girls got it, too). Luckily he was very fast in discovering this and treating the disease, resulting in him being able to come with us after all.

Much as the Wednesday before, thursday consisted mostly of driving. This time from Accra of course, to the east. The only stop we made was at the Akosombo Dam, the world's biggest free-standing dam.

![IMG_6629.jpg](/assets/img/ghana/IMG_6629.jpg){:.materialboxed}

It's construction created Lake Volta, the largest man-made Lake in the world by surface area and the third largest by volume. The dam houses a hydroelectric power plant that can produce up to 1020 megawatts (1370000 hp).

![IMG_6626.jpg](/assets/img/ghana/IMG_6626.jpg){:.materialboxed}

Unfortunately, as you can see in this picture, the water level is very low. It has been like this for over a year now. Actually, it is even below the minimum water level as defined during the construction of the dam and power plant. At this time, the plant is only producing 340 megawatts instead of the 1020 megawatts that make out 55% of the national demand. That's 37% of the national demand missing right there, on of the root causes of the reoccurring blackouts everywhere.

In the evening we reached the Waterfall Lodge in Afegame, a nice place run by a nice German couple where we stayed this as well as the following nights.

![20150830_173032_Ghana_John_IMG_3479.jpg](/assets/img/ghana/20150830_173032_Ghana_John_IMG_3479.jpg){:.materialboxed}

![20150831_101018_Ghana_John_IMG_3489.jpg](/assets/img/ghana/20150831_101018_Ghana_John_IMG_3489.jpg){:.materialboxed}

<h3>Friday</h3>

On Friday we went on our first hiking trip, visiting some caves in the mountains. John stayed at the lodge to cure his malaria while we got warmed up for the following days. We had a young and very friendly guide and joked about selling Olga to him, but we couldn't reach an agreement on the number of cows and goats she was worth.

The trip was very nice, in itself only about one and a half hours in total, but we went there by tro-tro and hiked back to make it four hours. We visited around 5 caves of which all were quite small and three not very deep. The other two were larger and all of us could enter them at once - to stand (or rather crouch) face to face with about a hundred bats!

![IMG_6699.jpg](/assets/img/ghana/IMG_6699.jpg){:.materialboxed}

![IMG_6700-e1441359437891.jpg](/assets/img/ghana/IMG_6700-e1441359437891.jpg){:.materialboxed}

![IMG_6684.jpg](/assets/img/ghana/IMG_6684.jpg){:.materialboxed}

On the way back we found a broken radio mast!

![20150828_134559_Ghana_John_IMG_3077.jpg](/assets/img/ghana/20150828_134559_Ghana_John_IMG_3077.jpg){:.materialboxed}

<h3>Saturday</h3>

On Saturday we climbed Mount Afadjato, the supposedly highest mountain in Ghana. It has the only drawback that its peak offers a clear view on an even higher mountain. How's that possible? Well we asked our guide and he told us that we should look closely: The other mountain actually sits on another mountain and therefore doesn't count. In itself it is not higher, it only "starts" further up than ours. uh-huh.

Anyway, it was a nice trip and John could actually come with us, as well as a trio from Hamburg we met at the lodge. Oh, and on the way back we passed a waterfall and Raffael and me took a swim below it.

![20150828_131913_Ghana_John_IMG_3062.jpg](/assets/img/ghana/20150828_131913_Ghana_John_IMG_3062.jpg){:.materialboxed}

![20150829_123303_Ghana_John_IMG_3118.jpg](/assets/img/ghana/20150829_123303_Ghana_John_IMG_3118.jpg){:.materialboxed}

<h3>Sunday</h3>

On Sunday we went on our longest tour, through the Wli Waterfall park.

![20150830_112416_Ghana_John_IMG_3272.jpg](/assets/img/ghana/20150830_112416_Ghana_John_IMG_3272.jpg){:.materialboxed}

We climbed the mountains on the right of this pictures and then steeply descended next to the waterfall, and then down to another waterfall below. The tour was exhausting and long but also very cool.

![G0321010.jpg](/assets/img/ghana/G0321010.jpg){:.materialboxed}

![20150830_152705_Ghana_John_IMG_3443.jpg](/assets/img/ghana/20150830_152705_Ghana_John_IMG_3443.jpg){:.materialboxed}

<h3>Monday</h3>

On Monday we went back to Accra. Originally we planned to go back on Sunday after the tour, but we were all oh so tired and the food was so good and the beds so comfy...

So we decided to stay one more night and leave on monday morning. After about half the way we had to say good bye to Olga, for she is staying in Ghana for 10 weeks in total and had to go back to Kumasi.

<h3>Tuesday</h3>

On Tuesday we brought John and Lisa to the airport. We had a last beer together and said goodbye to them, too.

Ben, Raffael and I went to Accra Mall afterwards, a huge mall that sells a lot of stuff we haven't seen for a while (cheese, chocolate, curved LED TVs for 26000 cedi...)

<h3>Wednesday</h3>

On Wednesday I had to say good bye to the two remaining guys. Raffael went back to Takoradi to (not) work for two more weeks while Ben stayed to leave later that same day.

I went to the Maranatha Beach Camp in Ada Foah again, where we have been around 6 weeks ago in the large IAESTE group already. I wanted to enjoy my last two days at the beach and so I did.

![20150719_153631_Ghana_John_IMG_1282.jpg](/assets/img/ghana/20150719_153631_Ghana_John_IMG_1282.jpg){:.materialboxed}

<h3>Thursday</h3>

Not much to say about it. Spend the day at the beach camp and went back in the evening.

<h3>Friday</h3>

Today. my last day. I leave for the airport in about 6 hours. I think I will spend most of my time reading now. Originally I planned to use it to plan my Dubai trip, because I will have a 28 hour transit stay in Dubai on my way back and use it to visit the city.

But my Dad seems to have already taken care of that, since he arranged for a Dubai Colleague of his to pick me up at the airport and show me around. I'm excited!
