---
layout: post
excerpt: Northern Trip part 2
---

On Friday, the second day of our northern trip, we went to the Mole National Park. Even the road to there felt wild and african: red dirt, potholes in which a lion could hide, almost glowing from the heat of the sun.

In spite of the open-topped Jeeps and other heavy safari equipment on the roadside, we were to conquer the area by foot. This was actually the first occasion in which I used my fancy hat, and lots of sun screen. But protection was not only needed from the top: An armed guide covered our sides and boots could be rented for 5 cedis. This didn't feel too necessary, since the ground was hot and dry and a little dusty at most. One detail made me wonder though: Our guide and all our IAESTE coordinators, who had done the trip multiple times before, got themselves some boots. Well, they didn't have any in my size anyway.

We started off walking to a small village, inhabited by various kinds of Lizards, Monkeys and Warthogs.

![IMG_6254.jpg](/assets/img/ghana/IMG_6254.jpg){:.materialboxed}

![IMG_6259.jpg](/assets/img/ghana/IMG_6259.jpg){:.materialboxed}

The guide told us that there are also lions and hyenas in the park. We were obviously most keen on seeing an elephant, though, and that's what we went looking for. I'm pretty proud to be able to honestly say I found the first one! Actually it was only minutes after we left the village. The guide had a place in mind and we were all heading there. I was in front of the group and looked back for some reason when I saw a big grey elephant butt disappearing behind a building. I told the guide and we turned around to track it down. We did!

![IMG_6278.jpg](/assets/img/ghana/IMG_6278.jpg){:.materialboxed}

![tmp_7449-IMG_20150710_083339-2052631926.jpg](/assets/img/ghana/tmp_7449-IMG_20150710_083339-2052631926.jpg){:.materialboxed}
Later on we also met a small elephant family.

![IMG_6282.jpg](/assets/img/ghana/IMG_6282.jpg){:.materialboxed}

They were drinking from a small puddle and that's where we get back to the boots. After we took a bazillion elephant pictures, the guide led us further down into a really muddy and wet area. The water got deeper and deeper until it was about 2 inches (5cm) high and us no-boot wearers could merely jump from one almost-dry spot to another. I took a long way around with the italian guy and managed to keep my feet somewhat dry, but when we found back to the group we saw that most had given up and went straight through the water.

We went to a small lake to see some crocodiles, but unfortunately without success. And that was it, for the most part. Besides, I think, we saw a goat on the way<br />
back to the bus. We cleaned our shoes at a tap outside on some camping area. As this was really necessary, I did it, too, and finally got my shoes also pretty wet in the process. Since we all had some form of sandals or flip-flops around, this was no problem at first. But when we wanted to leave, it got really uncomfortable inside the bus. The solution? we tied all the wet shoes to the rail on the top of our bus(outside), including socks and the lower legs of my zipping pants and more stuff. Great idea and we didn't lose any!

We left the park and on the way to Tamale, a rather big city in the north, we visited Larabanga Mosque.

![IMG_6288.jpg](/assets/img/ghana/IMG_6288.jpg){:.materialboxed}

The mosque was built in the early 15th century and is still a holy place to local muslims. Most of us were not allowed to enter the mosque, although one of the IAESTE coordinators and one student could enter as they followed the Islam. In Ghana, the Islam has spread from the northern border, while Christianity reached the country over the sea from the south. This origin still influences the distribution of Christians and Moslems in Ghana. The former hold the majority in the south, the latter in the north. This has not led to many problems, since Ghanaians are very tolerant and both groups are able to live together happily. It was also no problem for us to find food in the north, as it is still Ramadan.

In Tamale we stayed in a catholic hostel and went out partying pretty soon. We spent the rest of the night at a very cool rooftop bar above Tamale. One girl of our group also had her birthday that day, so we did not let her sleep afterwards and continued partying at the hostel until around 3am. John, an austrian guy from our group, drank way too much until I brought him to the room I was sharing with him. He fell asleep instantly.

And that was it for the day. On Saturday we had to get up at 6, and John having had only 3 hours of sleep led to some interesting situations, stay tuned!
