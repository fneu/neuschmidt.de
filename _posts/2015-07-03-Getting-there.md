---
layout: post
---

Getting to Ghana was the easy part. My flights with emirates were fine, and both planes featured movies, wi-fi and charging sockets including USB. I stayed at the Dubai airport for 6 hours in between 1 and 7 am and it was fine. I ate at Burger King for 26 UAE (that's important to my dad, as well as the fact that the movie was called Robot Overlords).

Yesterday at 11:30 I then reached the Accra international airport on time. I have never seen such a small airport, it was quite the change from Dubai. It isn't clear whether the landing strip is actually asphalted or only compressed dirt. Next to it there are a couple of small houses and some smaller airplanes spread around the scene. A bus brought me to immigration where the troubles began.

Without having a complete contact address memorized or on my phone, I needed to get my suitcase where I had it written down. I therefore had to pass security, for which I needed an immigration form, for which I needed a contact address... In the end I got to the baggage claim area by leaving my passport behind, but waiting another 30 minutes there for my suitcase didn't help to get it back. Overall it took about an hour for me to leave the airport, and my Visa was cut as expected to two months length. Which is 4 days short of what I needed, great. I'm not sure whether I did something right or wrong at this point to get the head immigration officer to give me his cell phone number, but it might be handy later.

After only 5 minutes of defending my suitcase against overly helpful taxi drivers I was actually picked up by Beatrice, the helpful IAESTE supervisor for Accra. I later learned that most students stay at her house for a night. Since I arrived so early, she did bring me directly to a bus station though. The taxi drive was quite spectacular and I was the source of some confusion when I changed to the other side of the back seat to buckle up.

Beatrice also helped me to buy some bare essentials, like water and a non-functional sim card for my phone, so that I could call my supervisor Dennis when I arrived in Kumasi. (By now I learned that new sim cards need some time to be registered, and I got an already registered one from another student that had the same problem a week ago). The bus ride was way longer than I expected (about 6 to 7 hours) and the roads where also much worse than anything I've ever seen

In both cities and close to the villages in between, there are people on the sides of the streets everywhere. They sell water, bread, fish, fruits, and lot's of other stuff. At one time, all the other people in the bus started to sing songs together. Seems like they do have a local language other than english because I didn't get a word. Couldn't ask anyone though because the guy next to me was asleep the whole ride.

I arrived at around 8pm in Kumasi and was picked up by Dennis, who brought me to the hostel. I share a Room with two other Students, and next door there are another 4. One is also called Dennis, from Sweden and he works at my company. There has been no electricity here for 8 days (actually we have lights besides the occasional power outages, but no electricity in the wall sockets for charging phones etc.). There is also no wi-fi, but with my new sim card I can use mobile internet which doesn't seem to be very expensive. We have a ventilator at the roof but no AC, the night was warm but ok.

All in all I feel neither confident nor particularly secure, everything is really different. Everybody is really nice though, and I think I will be fine.

Today I went to my new company with Dennis, ate Coco and Fufu and took my first few rides in a Tro-Tro, all of which I will tell you about later. I'm also sorry for not posting any pictures yet, they will follow. Right now I'm trying to charge my phone enough to send this somehow before I leave with the others to go somewhere...
