---
layout: post
---

Jep, that's it. I'm in the Bus now on my way to Accra and I'm not going to be back soon. Luckily I'm not alone. I had to say good bye to most people though, feels weird. Anyway, let's start where we left of. And please note that I'm writing this on my phone. Expect mistakes.

<h2>What happened so far</h2>

<h3>Adum again.</h3>

On saturday the 15th of August I was at Adum again, and boy is it big. so much bigger than what we saw last time on our own. This time I was there with Ben and his colleague and a specific target, but it was most important for his colleague to show us everything. Adum market is the largest market in west Africa.

![tmp_7877-IMG_20150815_152139-1515308508.jpg](/assets/img/ghana/tmp_7877-IMG_20150815_152139-1515308508.jpg){:.materialboxed}

<h3>Plush Lounge</h3>

Later that day we attended the opening ceremony of the Plush Lounge in Jubilee Mall close to our favorite Supermarket (Nutella....).

the ceremony was much bigger than we anticipated, there was a laser show, a red carpet, and a television team asking people for interviews.

![IMG-20150815-WA0013.jpg](/assets/img/ghana/IMG-20150815-WA0013.jpg){:.materialboxed}

The group pushed me a little because everybody trusted in my English, even under pressure. Thanks guys :) The interview was about music in general and somehow ended in all of us Germans singing the German national anthem because we didn't find another song that everybody knew.

The Lounge itself was crowded afterwards, but not very spectacular. The music was okay, the prices were not, it is definitely aimed at the richer people around here. The photographers were hopefully only there for the day. We left after a relative short time.

![tmp_7877-IMG_20150815_222524-1272559244.jpg](/assets/img/ghana/tmp_7877-IMG_20150815_222524-1272559244.jpg){:.materialboxed}

<h3>Football match</h3>

On Sunday we went to the football game of Kumasi Asante Kotoko against B. A. United. Both teams play in Ghana first league, Kotoko for Kumasi and the Ashanti region, B. A. United for the western region I believe.

Kotoko means porcupine :)

![tmp_7877-PANO_20150816_164539-1243618119.jpg](/assets/img/ghana/tmp_7877-PANO_20150816_164539-1243618119.jpg){:.materialboxed}

It was really hard to find out at what time the game would start and when we came they were already halfway through the first half and Kotoko was 2:0 in the lead. It got interesting though when United closed the gap and tied at the beginning of the second half. In the end we (Kotoko) won with 4:2 though.

<h3> Glasses</h3>

I got them fixed on Wednesday :)

![tmp_7877-IMG_20150818_130627976934433.jpg](/assets/img/ghana/tmp_7877-IMG_20150818_130627976934433.jpg){:.materialboxed}

<h3>Malaria</h3>

![tmp_7877-IMG_20150820_155404-500953504.jpg](/assets/img/ghana/tmp_7877-IMG_20150820_155404-500953504.jpg){:.materialboxed}

After the experiences others had at the hospital (expensive, long waiting queues, questionable results) I went directly to the pharmacy and got the same medicine everybody else got in these cases.

On Sunday my medicine was out and I still didn't feel very good. I decided to take my Malarone Prophylaxis in a higher treatment dosis for three more days as described in the information sheet that came with it. Today, on Wednesday, I made another test that was negative :)

Malaria Tropica can be completely treated to the point where it is not returning periodically like some other kinds of Malaria. I did make an appointment with a German doctor already so talk about this and other questions. Most likely I will not be allowed to donate blood for a very long time.

<h3>Shirts</h3>

On Monday I finally got the two custom made shirts I ordered at a Tailor here :)

![tmp_7877-IMG_20150824_093027-1268739746.jpg](/assets/img/ghana/tmp_7877-IMG_20150824_093027-1268739746.jpg){:.materialboxed}

![tmp_7877-IMG_20150824_1009351911253986.jpg](/assets/img/ghana/tmp_7877-IMG_20150824_1009351911253986.jpg){:.materialboxed}

<h3>Last day of work</h3>

Tuesday was my last day of work. It was rather unspectacular. After Lunch we made a bunch of pictures, though :)

All in all I had a very nice experience at Governor Steel. Thanks guys!

<h2>What happens now and soon</h2>

I'm in the bus to Accra right now, together with Ben, Lisa, and Olga from Germany and John from Austria. These guys came at almost the same time as I and will, with the exception of Olga, leave at almost the same time as I. They have become the core of "my" group here and I'm so happy we can go on this last trip together.

In Accra we will meet Raffael from Austria and together we will go on a trip to the Volta Region in the east of Ghana. We will see the big dam, Some waterfalls and climb a mountain.

On Sunday we will return to Accra and visit the city on Monday. Most of the others are then leaving on Monday and Tuesday, and Olga will go back to Kumasi. Raffael and I will stay and I'm not sure where I (or we) will spend my last days until I will leave Ghana on friday.

I'm sad to leave the others, but I'm so excited for the trip and secretly I'm also already very much looking forward to be back in Germany :)
