---
layout: post
---

Hey guys! As always I was planning on writing an article this weekend, and as always I did not find the time because of a trip. This weekend we went to the beach again, although another one this time.

<strong>Busua Beach</strong> lies in the western part of the Ghanaian cost, close to the city of Takoradi. All in all this weekend was pretty similar to the last one, the accommodation was a little bit better but the beach was not quite as nice. We could eat good and affordable in a small village nearby and different from last week we could leave the beach camp to see some of the attractions around.

Unfortunately, there was a lot more drama this time, starting with me cutting myself on the way out of our tro-tro. I did not notice it, but I must have touched something sharp in there while climbing from the last row to the sliding door. When I wanted to pay for the ride, blood was already dripping down my leg and I had a 4 cm cut right under the knee.

Later on Friday, Stella, the Greek girl, almost drowned in the sea after getting pulled out by a strong current. She was with Christian, one of the German guys, who was able to get my attention when I was relatively close by, surfing (more on that later). Together we tried to get and keep Stella on the surfboard and pull her back to the shore. Being trapped in the surge of some huge waves though, we were unable to do either. Christian then went back to the beach to alert the lifeguards, while I tried to keep Stella afloat and pull her further back and out of the breaking zone. The waves were too high and too frequent though, so we could merely brace ourselves for the next one and not really do much. The waves spun us around wildly and at some point we lost the board when it broke away from the string that attached it to my ankle. The water was shallow enough for me to stand in between the waves and I never really felt seriously endangered myself. I was really afraid to lose Stella, though, and <strong>very</strong> relieved to see the lifeguards coming with further boards. Together we managed to lift Stella on a board and pull her further away from the shore and out of the surge area. They then decided on the best way back and we made it back to the beach safely.

Far less thrilling but probably worse in the long run is that some guys are now complaining about flea bites. Until now we could not determine where exactly they got them, but fortunately it seems to not be our hostel in Kumasi. I'm very happy to not have any, since they are really itchy and might carry diseases.

After all it is worth noting, though, that we also had a lot of fun this weekend. The weather was very good and we made two trips on Saturday and Sunday, one to a castle with a great view and the other to a small village of fishermen built above a big lake. The food also was really good and affordable, we might have found the source of Ghana's best pancakes.

And we learned how to surf! At least a little. There was a surfing school right in front of the beach camp and it was not very expensive. I did not expect that I would ever get into surfing, but I gladly took the opportunity and had quite some success! Unfortunately no pictures from my side. And that's it for now. There will hopefully soon be another article about my work, though! :)
