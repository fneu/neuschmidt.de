---
layout: post
---

Let's talk about my work here in Ghana. A lot of people have asked me about it, and I think it's about time to introduce my workplace: Governor Steel Co. Ltd. in Ejisu, Ghana.

The Company produces Nails, steel roofing sheets of different kinds and Plastic rolls and bags. Why "Home of Fire products"? I don't really now. It has something to do with the tribe of the boss. Apparently they great each other with a friendly "fire!". Anyways, here is how the company describes itself on its flyer (original spelling):

<h2>Introduction:</h2>

![GS1.png](/assets/img/ghana/GS1.png){:.materialboxed}

<blockquote>
  GOVERNOR STEEL LONG SPAN ROOFING: It comes in various colors with elegance and nobility to brand your building with excellence style. The product is perfect and is founded by Advance Techniques, The brightness and glamorous nature give it another unique taste.
  
  These Long Span products produced by Governor Steel come in TILES, STRIPS, IBR, IDT and Concave-shaped roofing sheets. The products come along with well-designed RIDGE CAPS, RAIN GUTTERS, and VALLEYS etc. to enhance your building to perfection.
  
  Technically, professionalism at GOVERNOR STEEL is very accurate therefore special nails are used for the installation to make the roofing stronger and more beautiful, with the STRIPS, the nails do not appear directly thereby making the surface on the roofing very smooth and even in nature. With the rest, nail are covered with plastic caps of the some color of the sheets
</blockquote>

<h2>Company Site:</h2>

![PANO_20150716_085125.jpg](/assets/img/ghana/PANO_20150716_085125.jpg){:.materialboxed}

<h3>Main House:</h3>

![IMG_20150716_101707.jpg](/assets/img/ghana/IMG_20150716_101707.jpg){:.materialboxed}

This is where I work. The second and third floors of the building are not completed and are also not being worked on. The first floor is finished though and is functional. All the offices are in this building, including the general manager's office, the reception desk and the main office I'm working in.

All the windows are mirrors from the outside but see-through from the inside. Curiously, these windows are also used in all inside doors, including restroom doors. The main restroom is right behind the receptionist and you can see him and his computer screen from the inside... Can people from the other side see you? Yes, it's actually pretty easy to recognize the inside if you know what you are looking for.

<h3>Roofing department</h3>

The leftmost building is the biggest and contains the roofing department.

![IMG_20150716_101617.jpg](/assets/img/ghana/IMG_20150716_101617.jpg){:.materialboxed}

![IMG_20150716_101329.jpg](/assets/img/ghana/IMG_20150716_101329.jpg){:.materialboxed}

![IMG_20150717_085129.jpg](/assets/img/ghana/IMG_20150717_085129.jpg){:.materialboxed}

This is where the roofing sheets are made. I did not spend a lot of time in this part of the company, yet. On the last picture you can also see the store where our products are stored until they are sold. I like the roofing department because it doesn't smell and it is also not particularly loud.

<h3>Nail department</h3>

![IMG_20150717_170215.jpg](/assets/img/ghana/IMG_20150717_170215.jpg){:.materialboxed}

![IMG_20150717_170229.jpg](/assets/img/ghana/IMG_20150717_170229.jpg){:.materialboxed}

The nail department, located in the center, is loud. The machines here take steel wire with a diameter of several millimeters and cut it in pieces. A big Hammer also smashes the one side together for the typical head. We have seven of these machines and they sound like a dozen blacksmiths - each. Oh, and polishing tens of thousands of nails in a big drum is also not particularly quiet.

<h3>Rubber department</h3>

![IMG_20150717_134324.jpg](/assets/img/ghana/IMG_20150717_134324.jpg){:.materialboxed}

![IMG_20150717_170148.jpg](/assets/img/ghana/IMG_20150717_170148.jpg){:.materialboxed}

the rubber or plastic department is located on the right and it smells really bad. Production Process wise it is rather interesting though and you can actually talk to the people in the hall. Dennis and I spent most of our time <em>with</em> this department, although not necessarily <em>in</em> it if we could get around that. In here, new and so-called "virgin" plastic material is used as well as recycled material. The plastic is melted and transformed into bags and rolls.

<h3>Recycling site</h3>

![IMG_20150716_085233.jpg](/assets/img/ghana/IMG_20150716_085233.jpg){:.materialboxed}

The waste from the rubber department is made reusable at the recycling site. It gets melted here and cut into pellets. Not the most beautiful workplace you can imagine...

<h2>My Job</h2>

So what am I doing here?

Dennis and I usually work on task that have to to with the analysis and organization of the production processes. In example we gather performance data of workers at different stations in the rubber department and create a weekly performance report for the general manager. We also analyzed how much waste we create in the nail production and calculated the yield of nails we get from one 2-ton-coil of steel.Our current task is to reduce the amount of unfinished products that are stored at various places in the halls and to reduce the creation of waste where we can.

Sounds cool, eh? It is, and I feel that we can actually contribute value to the company. This does not mean that we work a lot or anywhere near full-time, though.

<h3>A usual work day</h3>

Work starts at 8 am on weekdays and weekends are free. I work for 8 hours a day with a formal break of one hour so I stay until 5 pm.

<ul>
<li>Work usually starts at <strong>8 am</strong> with taking a small tour around the site, greeting everybody. Afterwards we usually share some breakfast if we brought peanuts or fruits or other food with us.</p></li>
<li><p>At around <strong>9 am</strong> we start to work on a current topic. Work is only interrupted every couple of minutes by small conversations but sometimes goes uninterrupted for quite some time. This is the productive time of the day.</p></li>
<li><p>At around <strong>11 am</strong> everything slows down to a crawl as we slowly approach lunch time. We sometimes talk with our colleagues or read the news or chat with our friends at home.</p></li>
<li><p>At <strong>12 pm</strong> we have Lunch break. Because we work quite far outside of Kumasi, we don't have that many options for food. We usually go to a place nearby where we can get Fufu, Banku or Rice balls and Soup. I will write more about the food in this country later. We usually talk with the cook for a while afterwards and sometimes go to the hospital to buy some fruits (fresh mango, pineapple, peanuts, bananas).</p></li>
<li><p>At around <strong>1:30 pm</strong> we find ourselves back at the workplace and we share some of our fruits, talk to colleagues, relax.</p></li>
<li><p>Sometimes, we go for a second round of work at around <strong>2 pm</strong>, although we rarely work for more than one of the last 3 hours.</p></li>
<li><p>At around <strong>3:30 pm</strong> we finish for the day and kill time until we can leave</p></li>
</ul>

<p>Now, please note that this is only an average day. Sometimes we work until 5 pm. Today I didn't really do anything besides writing a couple of blog post. TIG - This is Ghana!

<h2>Electricity</h2>

Sometimes it's there, sometimes it's not. It's only Tuesday but the week has been good so far. On Monday we had only one blackout for 47 minutes at around noon which didn't affect us too much. Today not a single one. Usually, we experience around 2 blackouts a day, they are called "Lights out" over here. Last week we didn't have electricity for two whole days.

The reason seems to be the rapid growth of the Ghanaian industry and the inability of the local providers to provide enough energy. If the demand for electricity is to high, some districts of Kumasi get more or less randomly switched off. There is no warning and not much of a pattern behind it.

<strong>zooosh - post out</strong>
