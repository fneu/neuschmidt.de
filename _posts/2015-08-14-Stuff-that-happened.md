---
layout: post
---

Hey guys! I'm sorry for not writing during the last weeks. A lot of stuff happened; I wanted to write a couple of articles but didn't find the time. Now it's quite the pile and I feel unable to keep up, so I will just try to mention everything that happened until now in one post and be done with it.

<h3>Adum</h3>

Two weeks ago, on Wednesday, I went to Adum with a couple of guys. That is so long ago that Dennis was still with us. Adum is a central district of Kumasi. It has a lot of businesses and a huge market. Dennis kindly offered to take us there and show us a shop where he had bought some souvenirs and got friends with the owner. We hoped to get good prices because of that - and did, but I'm not yet ready to reveal what I bought there ;)

Anyways, the shops where close to the business area, but of course we wanted to see the market, too. So we went there:
![PANO_20150729_174304_1_2_3.jpg](/assets/img/ghana/PANO_20150729_174304_1_2_3.jpg){:.materialboxed}

The market is <em>huge</em>. On the picture you can see only a tiny fraction and Tro-Tros are only present on a couple of streets. The sidewalks are covered with several rows of merchants in front of the shops in the buildings. You can buy pretty much anything here, it's just important to know your way around to know where to go. So unless one wants to only look around as we did, it is advisable to meet someone there who can show you the way.

A little orientation is provided by the macrostructure of the shops, there is an area for tourist, one for used clothes, and generally richer areas where merchants offer smartphones, laptop chargers, jewelry, and shoes, and poorer areas where you can buy kasawa and jams, feature phones, and used phone batteries.

It was interesting and very nice to go there, and by now I've actually been to Adum a couple of times for different reasons. Always with a guide and a specific target though. You can really buy everything here.

<h3>Rosicrucian Fellowship</h3>

In the same week, my boss Johnny offered to take me to church with him on Sunday. I think I wrote about church before, the last church I was at was very lively and close to what I remember about church from the US. Drum set and gospel choir in the front, song texts thrown on a couple of big screens by beamers, and a cheering and dancing crowd of a couple hundred people.

Johnny promised me something very different, and he was right. The church he took me too is a church of the <a href="https://en.wikipedia.org/wiki/Rosicrucian_Fellowship">Rosicrucian Fellowship</a>, "An International Association of Christian Mystics". As far as I understand, they incorporate esoteric Christianity, Philosophy, "spiritual Astrology", and Sciences into their belief. The church is open to all who are not professional hypnotists, mediums, palmists or astrologers. As I am none of these, I was happily welcomed.

The service was rather "european": Almost completely in English, with a lengthy preachment and some songs from a songbook accompanied by an organ. Well, an electric keyboard set to "organ" with relatively weak speakers, amplified by the speaker's microphone. This is Ghana after all. A difference to our german church was definitely the discussion that followed the preachment. It was largely not held in english though, so I didn't understand anything. It was a nice experience anyways, so thank you Johnny for taking me with you!

![IMG_20150802_124412_1.jpg](/assets/img/ghana/IMG_20150802_124412_1.jpg){:.materialboxed}

<h3>Cape Coast</h3>

The week was rather standard, there's not a lot to tell you about it. Last weekend we went on the last by IAESTE organized trip - to cape coast! Cape coast is a city at the coast in the south of Ghana. It has nice beaches and if I remember correctly 5 castles, the most famous of which is undoubtedly Cape Coast Castle, which we visited:

![IMG_6441_1.jpg](/assets/img/ghana/IMG_6441_1.jpg){:.materialboxed}

![IMG_6407_1.jpg](/assets/img/ghana/IMG_6407_1.jpg){:.materialboxed}

Originally built by the Swedes for trading gold, it got it's terrible fame later on when it was used for the Atlantic slave trade mainly by the Brits. Several hundred slaves were held here at a time, brought from all over the country to leave it through the castle's "Door of no return". The slaves were held in underground dungeons, only able to see the sun through the little holes in the ceiling that enabled numerous guards to watch them. "Medical treatment" was provided by the nearby sea in which sick slaves were thrown when they did not recover quickly enough. The castle is a terrible but important proof of the unbelievable crimes that were committed in this country during 400 years of slave trade.

We stayed the night at a beach camp quite close to the castle, with nice but expensive food and lots of pushy children that wanted to sell banana chips or groundnuts for a few cedis. It really felt like a tourist place but it was okay for a day. We slept in one big room and the beds were fine, if short, and everybody was provided with a mosquito net and a wall socket. (Both are invaluable for the quality of life over here).

![IMG_6396_1.jpg](/assets/img/ghana/IMG_6396_1.jpg){:.materialboxed}

<h3>Kakum National Park</h3>

On Sunday, most of us joined the trip to Kakum National Park. This park is unique for it was established by the initiative of the local people and not the State Department of wildlife like the other national parks. It is covered with tropical rainforest and features a canopy walkway with an awesome view over the forest.

![IMG_6461_1.jpg](/assets/img/ghana/IMG_6461_1.jpg){:.materialboxed}

![IMG_6459_1-e1439567005128.jpg](/assets/img/ghana/IMG_6459_1-e1439567005128.jpg){:.materialboxed}

![IMG_6477_1.jpg](/assets/img/ghana/IMG_6477_1.jpg){:.materialboxed}

After crossing the seven walkway bridges without any further security equipment, a few people and I joined the guide for a "nature walk". For this walk we left the main trails for a couple of narrow paths through the jungle. The guide showed us many different plants and explained their features and uses. It's really hard to write about, but it was awesome to see, a little magical somehow, really nice!

![IMG_6503_1.jpg](/assets/img/ghana/IMG_6503_1.jpg){:.materialboxed}

![IMG_6505_1-e1439567589647.jpg](/assets/img/ghana/IMG_6505_1-e1439567589647.jpg){:.materialboxed}

![IMG_6512_1.jpg](/assets/img/ghana/IMG_6512_1.jpg){:.materialboxed}

<h3>Tailor</h3>

Back in Kumasi, I finally got to do something that I wanted to do long before and that slowly turned urgent: Go to a tailor to order a custom-made shirt! Dennis has done this before, but it was an odyssey that took him about 5 weeks and almost left him without shirt at all. Fortunately my boss offered me his support to find a good and trustworthy tailor who actually does and delivers work. So on Tuesday after work we went there together to get my measurements down and decide on fabrics and stuff.

Funnily enough I didn't feel very involved in the design and decision-making process, mainly because the tailor was mostly speaking in the local language and my boss translated some, but not all of it. <strong>I</strong> decided to order two shirts, a long and a short-sleeved one. The <strong>tailor</strong> decided that he wanted to use a nicely structured but plain white fabric for the long-sleeved shirt, together with one plain colored fabric of my choice. <strong>He</strong> also decided where in Adum I could buy a fabric of my liking for the short-sleeved shirt. By now I have the fabric and my boss will give it to the tailor, I'm excited to see what he makes of it as I have no idea. I paid about 110 cedis for the work and the materials, which is equal to about 27€ and a lot of money in Ghana.

<h3>Jennifer</h3>

Like everything in Ghana that is supposed to last <em>just</em> an hour, we took a lot longer than expected (by my boss) to reach the tailor and get everything set there. That was a little unfortunate because I was to meet Jennifer that same evening. Jennifer is a student of my mother in Germany, but she has family in Ghana and went to visit them this summer.

It's an incredible coincidence that she stayed at the same district the tailor was at, quite far outside the city center and far away from my hostel. So we met at her place spontaneously and talked about life in Ghana and Germany and what we had to get used to over here.

Her family way very nice to me and took got care of me to get me safely home that night. They had also previously invited me for the next day to join them on a trip to the center of Ghana. So, on Wednesday morning they picked me up at my hostel and together with her whole family we went north in an old sprinter. It was one of these buses that fit 22 passengers but also sometimes 24 if need be and I think we were a couple more than 22. I do not remember all the people I met, but it was her, her parents, a couple of aunts and uncles and cousins and siblings, some of them my age, some above, some below.

Together we went to the Kintampo Waterfalls and the center of Ghana. I almost made it to both places on the northern trip, but at that time we decided against it to reach Kumasi earlier. So I'm glad I got the possibility again, and even though it was a long ride it was fun and worth it.

![tmp_20063-IMG_20150812_124901373853314.jpg](/assets/img/ghana/tmp_20063-IMG_20150812_124901373853314.jpg){:.materialboxed}

![tmp_20063-IMG_20150812_124908-226416777.jpg](/assets/img/ghana/tmp_20063-IMG_20150812_124908-226416777.jpg){:.materialboxed}

![tmp_20063-PANO_20150812_153239769437923.jpg](/assets/img/ghana/tmp_20063-PANO_20150812_153239769437923.jpg){:.materialboxed}

Afterwards we went to a place I had been to before, the Monkey Sanctuary we visited on the northern trip! It was very cool to go there again. The guide was the same as before but didn't recognize me. But he has a lot of visitors and white people look probably all the same to him, so he is forgiven ;) The monkeys probably didn't recognize me either, but they were closer to and in the village this time and equally eager for groundnuts and bananas as last time. It struck me again how smart they are. If you offer them food and take it away at the last moment, they will turn away from you and don't take food from you anymore.

All in all it was a very nice day and I felt well cared for. Thank you for taking me with you! Und Jennifer, grüß die Familie wenn du das hier liest ;)

<h3>Glasses</h3>

My glasses broke D:

![IMG_20150809_063703_1.jpg](/assets/img/ghana/IMG_20150809_063703_1.jpg){:.materialboxed}

My boss tried to fix them and it looked good for a while:

![tmp_20063-IMG_20150810_175737-1312364151.jpg](/assets/img/ghana/tmp_20063-IMG_20150810_175737-1312364151.jpg){:.materialboxed}

But now they broke again. I'm not sure yet what we can do. I would rather not glue the glass straight to the frame, but maybe we have to. For now I'm doing well without, but it feels weird and is not exceptionally comfortable.

<h3>Time</h3>

Time's almost up for me! The feeling of having to leave soon came pretty much at the time where Dennis left and I had the first half of my stay completed. By now another two weeks passed and it feels like it's getting really close. As I wrote before I will go on one last trip with some other guys at the end, starting on August 27th or maybe even 26th. That means I have less than two weeks left in Kumasi, and only 7 or 8 days of work! I will have to start with my internship reports for IAESTE and this company next week, so that I can finish my last weekly report on the last Monday and then it's time to say goodbye already - wow!

There is also a lot of stuff to do now that I wanted to do "before I/we leave" like getting a shirt made (i hope the tailor finishes in time), buying some bracelets and jewelry from the friend of a work colleague as I promised him, going to the golden Tulip hotel and try their buffet. Oh, and writing another blog post about the food here!

If there are other things that you want me to write about, this is probably the time to tell me! Shoot me an email or comment at the bottom of this post, it really takes a long time to write this up and sometimes I need pictures so please tell me now!

And so, that's it again, I hope you enjoyed the read!

See you, guys, and soon in person!
