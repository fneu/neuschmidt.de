---
layout: post
---

Sorry for the long absence, guys! Writing a blog takes more time than I thought, and I'm usually pretty tired after work. Originally I wanted to spend some time writing this weekend, but I had no idea that we would be doing a surprise trip for Ben!

Ben is the german guy that arrived a couple of days before me (I really have to introduce all of them here at some point). He is also the youngest one, I learned, as he turned 21 on Saturday. We had a party for him from friday night to Saturday, starting at the hostel with some Watermelon drink, beer and Whiskey bags:

![tmp_2909-IMG_20150717_2114502123479156.jpg](/assets/img/ghana/tmp_2909-IMG_20150717_2114502123479156.jpg){:.materialboxed}
I have not seen a lot of bottles in this country, especially not glass ones.

We later went to two night clubs in Kumasi, although the second one was closed so we arrived back at the hostel at 1:30 am <em>already</em>. I remember some remark of Ben that we were finally able to sleep in as long as we wanted and that we would have another party the next night. Little did he know of the secret plan we had... 4 hours later, the other Students stormed our room, singing Happy Birthday as well as banging pans and dishes against his bed. He had about an hour to get up and pack his stuff for a 3-day trip - to Maranatha Beach Camp in Ada Foah!

![IMG_6353.jpg](/assets/img/ghana/IMG_6353.jpg){:.materialboxed}

It is located in the south of Ghana on a peninsula that separates the river Volta from the Atlantic Ocean - up to the point where they meet.

It comes very close to the typical Paradise Island: Coconut Palms, Sea, Beach, Sun, Tropical Huts and bad sanitation... With the added benefit of two beaches a hundred meters from each other, of which one features the very warm and calm water of the Volta river and the other the blustering surge of the atlantic ocean waves.

We met the group from Accra there as well as Raphael from Takonadi. A lot of new guys also arrived, 4 of them will stay in Kumasi with us. We stayed at very basic huts which consisted only of a roof and very thin walls. The beds stood on the sand that would soon be everywhere, and the doors were locked by the 
![IMG_6387.jpg](/assets/img/ghana/IMG_6387.jpg){:.materialboxed}
smallest locks I have ever seen. It didn't matter too much though as we really didn't spent much time inside.

It was for sure the most relaxed weekend I had so far and I really enjoyed it. Monday was a holiday in Ghana, so we could stay for 3 days until we had to take go home by bus and Tro-tro. On the way back we almost lost the new guys that had all of their stuff in Accra, but it all worked out. And that's it for now. Pictures:
(I'm sorry that most of them are taking in the early morning with bad weather)

![tmp_2909-IMG_20150718_16512321304934376.jpg](/assets/img/ghana/tmp_2909-IMG_20150718_16512321304934376.jpg){:.materialboxed}

![tmp_2909-IMG_20150718_165007300311996.jpg](/assets/img/ghana/tmp_2909-IMG_20150718_165007300311996.jpg){:.materialboxed}

![IMG_6390.jpg](/assets/img/ghana/IMG_6390.jpg){:.materialboxed}

![IMG_6388.jpg](/assets/img/ghana/IMG_6388.jpg){:.materialboxed}

![IMG_6365.jpg](/assets/img/ghana/IMG_6365.jpg){:.materialboxed}
