---
layout: post
excerpt: Northern Trip part 3
---

On Saturday morning the bus was supposed to leave at 6. Some people begged for a fourth hour of sleep after the party, but there was no talking to Dennis and the other coordinators. So I had set my alarm to 5:30. I took a shower, ate some mango and was done by 6. Since we stayed the following night in the same hostel, we didn't have to pack our stuff together, which was nice. During the 30 minutes that I was awake, I woke John up about 5 times and brought him near consciousness twice. By 6 he slowly started to get rolling, putting clothes on and so on.

I went out to look for the bus, but there were just some other guys waiting, no bus in sight. This is Ghana, what was I thinking? I went back to John and checked again at 6:10 and 6:20. No bus. When I went out at 6:35, there was still no bus, but also nobody left waiting for it. Did they really just leave without us? This was especially weird because on Friday we waited about half an hour for 3 people to make it to the bus. Neither me nor John had a lot of telephone numbers, but we were able to call one of the guys from Kumasi and he told us that they were indeed already on their way, but they stopped at the hospital because one guy got malaria on the trip.

John and I hurried to get a taxi to get to the hospital before they leave again. We actually managed to do so, but it turns out they didn't forget us and were going to pick us up after going to the hospital. They didn't bother to tell us though, what do we have a IAESTE Ghana Whatsapp group for?

Anyways, we made it to the bus and enjoyed a couple of hours of bus ride, until we finally arrived at the place that everybody was talking about before the trip: The Paga Crocodile Pond where you can ride on crocodiles:

![IMG_6307.jpg](/assets/img/ghana/IMG_6307.jpg){:.materialboxed}

The crocodile in the picture is actually hungry and not fed as we thought it would be. The most modern strategy of crocodile riding seems to be to keep a tasty bird in range to distract the crocodile if it get's too interested in humans. We also didn't really "ride" it, but merely squatted over it and touched it's back, pretty cool anyways, though!

Amazing creatures, and we did pay them some respect:
![IMG_6321.jpg](/assets/img/ghana/IMG_6321.jpg){:.materialboxed}

![IMG_6323.jpg](/assets/img/ghana/IMG_6323.jpg){:.materialboxed}

According to the guide there are about 2000 crocodiles in the pond, 15 of which are trained for riding.

After the Crocodile pond, we went to a former slave camp in the area. We learned a lot about how slaves were gathered here, sold, and sent to cape cost for transportation to europe or america. I didn't know, for example, that most of the lowest level slave traders were black. The pits in the stone that you can see in the picture has been grinded by slaves to eat from.

![IMG_6329.jpg](/assets/img/ghana/IMG_6329.jpg){:.materialboxed}

Since we were so far up north already, we couldn't go back without paying at least a very small visit to Burkina Faso, the country in the north of Ghana. There is a small strip of no mans land in between the countries and we could go there, but we weren't allowed to cross the border to Burkina Faso without paying 10,000 something for a Visa. I don't know what currency they have over there and what it is worth, but it sure sounds expensive!

![tmp_7449-IMG_20150711_141147-1232300759.jpg](/assets/img/ghana/tmp_7449-IMG_20150711_141147-1232300759.jpg){:.materialboxed}

Anyway, we went back to the hostel afterwards and some people spent another night partying on the rooftop, but I stayed at the hostel bar with a couple of guys. We talked german with each other and watched parts of Fast&amp;Furious 6 on TV - we all went to sleep before the movie ended.

The sunday wasn't too interesting, actually, so I will cover it here very quickly. Basically we went back to Kumasi and we had to do so early enough for the guys from Accra to make it to the capital on the same day. The only thing we visited were some waterfalls which were actually pretty cool.

Not of the big, cold Scandinavian kind, but warm, wide and relatively small. We swam in the water above and below it, took some very nice pictures (not with my camera, though) and had some fun climbing it up and exploring the river around it. In the picture you can see that there is one part in the back of the waterfalls that is actually pretty high (5 meters?). We climbed it up, too.

![IMG_6338.jpg](/assets/img/ghana/IMG_6338.jpg){:.materialboxed}

Afterwards we went home to Kumasi, had some food and went to sleep pretty early, because another week of work awaited us.

Actually, I really want to tell you about my work soon, this will probably my next topic. see ya!
