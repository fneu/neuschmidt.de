---
layout: post
---

Hey guys! As most of you know, I'm not the type of guy that is on facebook or Google+ a lot. Therefore I have used various communication channels in the past, to talk to you and keep you updated on what is happening in my life. This will not change. But it didn't work so well in all cases and some of you haven't heard from me in some time because I was missing an easy way to spread information to all of you at once.

Since I will be abroad again for a while now I decided to do something about it yesterday, and thus this blog was born. Let's try this. I'm excited. I will try to keep you updated during my stay in Ghana (see next post) and post pictures and stuff here. Feel free to leave some comments. If this works out for you and me, I will continue writing.

And now go read the other posts, they are probably more exciting ;)

