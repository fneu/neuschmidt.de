---
layout: post
---

I have not yet introduced all the students here, so I will do that now very quickly. They are sorted by their date of arrival in Kumasi:

<h3>Greg</h3>

![IMG_6303.jpg](/assets/img/ghana/IMG_6303.jpg){:.materialboxed}
<em>Nick</em>: King of the north<br>
<em>Country</em>: Scotland<br>
<em>About him</em>: Crazy guy that can make the weirdest faces. Surprisingly understandable english. He brought a kilt and a Scottish flag with him. The flag stayed at the beach camp though, where it hangs higher than the union jack. He voted yes for Scottish independence. He was alone here during his first week and unfortunately he will leave at the end of this week already.

<h3>Dennis</h3>

<em>Nick</em>: Big D<br>
<em>Country</em>: Sweden<br>
<em>About him</em>: Dennis works with me and showed me around in the first weeks. He has always be my go-to guy for any problems I had. He is very good at motivating people and he keeps the group together. He started a little group that meets to go jogging in the morning. Has a beard like a Viking :) To bad he had Malaria and couldn't attend the northern Trip.

<h3>Pablo</h3>

![IMG_6305.jpg](/assets/img/ghana/IMG_6305.jpg){:.materialboxed}
<em>Nick</em>: <a href="https://en.wikipedia.org/wiki/The_Stray_Cat">La Gata</a><br>
<em>Country</em>: Spain<br>
<em>About him</em>: Best buddy of Greg and competitor for the weirdest-facial-expressions award. Always there but not necessarily in the foreground. Very good at basketball, football and ping-pong!

<h3>Ben</h3>

![IMG_6302.jpg](/assets/img/ghana/IMG_6302.jpg){:.materialboxed}
<em>Nick</em>: Bishop<br>
<em>Country</em>: Germany<br>
<em>About him</em>: Arrived a couple of days before me and sleeps in my room. Comes from Bavaria but can speak very understandable german! Together with John we form a core group and "the german room". We get along very well.

<h3>Fabian</h3>

![IMG_6311.jpg](/assets/img/ghana/IMG_6311.jpg){:.materialboxed}
<em>Nick</em>: First one to not have one. Maybe I will get one soon?<br>
<em>Country</em>: Germany<br>
<em>About me</em>: I don't think I changed much, maybe I got a little tanned?

<h3>John</h3>

![IMG_6299.jpg](/assets/img/ghana/IMG_6299.jpg){:.materialboxed}
<em>Country</em>: Austria<br>
<em>About him</em>: Not the Austria with the Kangaroos. Arrived one day after me. He completes "the german room". We really get along very well and we also stayed together in one room on the northern Trip. John is the cameraman, he takes the most pictures with his two nice cameras, one of which is a waterproof go-pro! John has been on the most amazing trips (Kilimanjaro) and he has been to Africa before. He does cross fit at home and makes us work out in the evenings.

<h3>Lisa</h3>

<em>Country</em>: Germany<br>
<em>About her</em>: The first girl to arrive in Kumasi this year. Lisa is from Hamburg, too, and got her Bachelor's Degree at my University. She went to Karlsruhe, though, for her Master's. Lisa has been a scout for years and she brought a lot of useful things (especially a solar-powered lamp!). She seemed a little shy at the beginning but she settled in very fast. She reads this blog so I have to be careful here ;) . Unfortunately she did not want to ride the Crocodile, so no picture for now.

<h3>Olga</h3>

![IMG_6314.jpg](/assets/img/ghana/IMG_6314.jpg){:.materialboxed}
<em>Country</em>: Germany<br>
<em>About her</em>: Came at the last minute to join the northern trip. She seems to get along very well with everyone and she has been an integral member of the group from the very start.

<h3>Stella</h3>

<em>Country</em>: Greece<br>
<em>About her</em>: Stella arrived when we were at the beach camp in Ada Foah. She has not been with us for long but she really tries to integrate herself into the group quickly. She seems to have brought the right thing for every situation, including medicine for my throat that was sore after the trip.

<h3>Bard</h3>

<em>Country</em>: Poland<br>
<em>About him</em>: Also arrived when we were in Ada Foah. The very first thing we talked about was stealing cars (a common prejudice in germany). In Ada Foah he stayed with the people from Accra for the most part, so we didn't get to know each other much until now.

<h3>Christian</h3>

<em>Country</em>: Germany<br>
<em>About him</em>: Also arrived last weekend. Comes from Dresden. Looks like the typical football(soccer) player (tall, blond, sporty) and he seems very nice.

<h3>Dimitri</h3>

<em>Country</em>: Serbia<br>
<em>About him</em>: The fourth one that arrived this weekend. Seemed a little shy to me as he didn't say much. I have yet to get to know him.

honorable mentions:

<h3>Dennis</h3>

<em>Nick</em>: Classic Dennis<br>
<em>Country</em>: Ghana<br>
<em>About him</em>: Our IAESTE coordinator. Made a very good job organizing the northern trip. Not always present, though. I'm sometimes not yet sure how reliable he is, but he is there when it counts.

<h3>Aaron</h3>

<em>Country</em>: Ghana<br>
<em>About him</em>: Lived in our room with us until last week. Didn't talk to us much until the northern trip. He left for a weekend trip last week and never came back until now...

<h3>Frank</h3>

<em>Nick</em>: King Kong King (and lot's of other names)<br>
<em>Country</em>: Ghana<br>
<em>About him</em>: Supports IAESTE when he is needed. His main business is currency exchange and he can be very helpful with that. He is very entertaining if a little vulgar at times. He shows up and leaves when he wants to.

<h3>Cristina</h3>

<em>Country</em>: Canada (Romania)<br>
<em>About her</em>: Cristina was born in Romania but she has lived in Canada now for more than 11 years. She is not an IAESTE intern but she works for a NGO in Kumasi. Cristina studies law now, after finishing her degree in philosophy. She met (classic) Dennis in Kumasi and joined the northern Trip. We have seen her a couple of times since, but she is leaving Ghana soon.
